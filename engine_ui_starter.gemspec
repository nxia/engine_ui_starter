$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "engine_ui_starter/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "engine_ui_starter"
  s.version     = EngineUiStarter::VERSION
  s.authors     = ["nxia"]
  s.summary     = "Summary of EngineUiStarter."
  s.description = "Description of EngineUiStarter."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails"

  s.add_dependency "bootstrap-sass", "~> 3.3.6"
  s.add_dependency 'font-awesome-rails'
  s.add_dependency 'devise'

end
