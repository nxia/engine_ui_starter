require 'test_helper'
require 'generators/engine_ui_starter/install/install_generator'

module EngineUiStarter
  class EngineUiStarter::InstallGeneratorTest < Rails::Generators::TestCase
    tests EngineUiStarter::InstallGenerator
    destination Rails.root.join('tmp/generators')
    setup :prepare_destination

    # test "generator runs without errors" do
    #   assert_nothing_raised do
    #     run_generator ["arguments"]
    #   end
    # end
  end
end
