require 'test_helper'
require 'generators/engine_ui_starter/devise/devise_generator'

module EngineUiStarter
  class EngineUiStarter::DeviseGeneratorTest < Rails::Generators::TestCase
    tests EngineUiStarter::DeviseGenerator
    destination Rails.root.join('tmp/generators')
    setup :prepare_destination

    # test "generator runs without errors" do
    #   assert_nothing_raised do
    #     run_generator ["arguments"]
    #   end
    # end
  end
end
