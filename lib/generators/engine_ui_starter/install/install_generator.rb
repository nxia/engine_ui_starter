class EngineUiStarter::InstallGenerator < Rails::Generators::Base
  source_root File.expand_path('../templates', __FILE__)

  def install_javascripts
    inject_into_file 'app/assets/javascripts/application.js', "//= require engine_ui_starter\n", before: "//= require_tree ."
  end

  def install_stylesheets
    inject_into_file 'app/assets/stylesheets/application.css', " *= require engine_ui_starter\n", before: " */" 
  end
end
