class EngineUiStarter::DeviseGenerator < Rails::Generators::Base
  source_root File.expand_path('../templates', __FILE__)

  def run_devise_generator
    generate "devise:install"
    generate "devise", "User"
    rake "db:migrate"
    generate "devise:views"
  end
end
