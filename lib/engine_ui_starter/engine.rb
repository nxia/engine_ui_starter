module EngineUiStarter
  class Engine < ::Rails::Engine
    isolate_namespace EngineUiStarter

    puts "========"
    puts File.expand_path("../../../vendor/assets/stylesheets", __FILE__)

    config.assets.paths << File.expand_path("../../../vendor/assets/stylesheets", __FILE__)
    config.assets.paths << File.expand_path("../../../vendor/assets/javascripts", __FILE__)
    config.assets.precompile += %w( engine_ui_starter.scss )
  end
end
